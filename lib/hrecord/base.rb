
module HRecord
  class Base
    
    attr_accessor :tablename, :cfnames, :server, :row_key, :schema
  
    @max_cell_verisons = 1
  
#    params: {row_key: '<uuid>', :'data:col'=>'value'}
#    params: {data: {col: 'value'}}
    def initialize(row_key, **params)
      self.tablename = self.class.tablename
      self.server = self.class.server
      self.cfnames = self.class.cfnames
      
      params = params.with_indifferent_access
      new_params = {row_key: row_key}
  	  params.each{|p,pv| pv.each{|k,v| new_params["#{p}:#{k}"]=v }}
  	  
      puts ({row_key: row_key, params: new_params})
      
      if !params.empty?
        apply_row(Row.from_params new_params.with_indifferent_access)
      end
    end
    
    def self.migrate
      #add checking for table already created or in need of updating
      if !@server.tables.include? tablename
#        puts "creating table '#{tablename}' with column families #{cfnames}"
        @server.create('table', tablename, cfnames)
      else
#        puts "table '#{tablename}' already exists"
      end
    end
    
    def self.drop
      @server.delete(tablename)
    end
    
    def self.create(row_key, params)
      created = self.new(row_key, **(params.symbolize_keys))
      created.save
    end
    
    def self.destroy_all
      all.each do |record|
        record.destroy
      end
    end
    
    def update(params)
      params.each do |key, val|
        key = key.to_s
        cf, qual = key.to_s.split(':')
        eval("self.#{cf.pluralize}[qual] = val")
      end
      save
    end
    
    def destroy
      @server.delete tablename, row_key
    end
    
    # TODO: improve this!
    def is_valid?
      valid = false
      
      # valid if not blank
      cfnames.each do |cf|
        if eval("self.#{cf.pluralize}.size > 0")
          valid = true
        end
      end
      
      return valid
    end
    
    def save
      if is_valid?
        server.put tablename, to_row
        apply_row @server.scan(tablename, args: {startrow: row_key, limit: 1, maxversions: @max_cell_verisons})[0]
      else
        return false
      end
    end
    
    def apply_row(row)
      raise StandardError.new("row does not exist") if row.nil?
      cells = row.cells
      cfdata = {}
      cells.each do |cell|
        cf, qual = cell.column.to_s.split(':')
        cfdata[cf] ||= {}
        cfdata[cf][qual] = cell
#        tsvalue = TSValue.new(cell.value, cell.timestamp)
#        cfdata[cf] ||= {}
#        cfdata[cf][qual] ||= TSArray.new
#        cfdata[cf][qual].array.push(tsvalue).sort
      end
      
      @row_key = row.row_key
      
      cfnames.each do |cf|
        $attribute_name = cf.pluralize
        
        class << self
          attr_accessor $attribute_name#.to_sym
        end  
        
        data = cfdata[cf].nil? ? {}.with_indifferent_access : cfdata[cf]
#        data = if cfdata[cf].nil? then {} else cfdata[cf] end
        eval("self.#{$attribute_name} = data")
      end

      self
    end
    
    def self.from_row(row)
      raise StandardError.new("row does not exist") if row.nil?
      object = self.new(row.row_key)       # self refers to the class when used in a classmethod
      object.apply_row row
    end
    
    def to_row
      cells = []
      cfnames.each do |cf|
        cfdata = eval(cf.pluralize)
        cfdata.each do |qual, val|
          if val.is_a? Cell
            cells << val
          else
            cell=Cell.new(column: "#{cf}:#{qual}", versions: {0=>val.to_s})
            cells << cell
          end
        end
      end
      Row.new row_key: row_key, cells: cells
#      Row.new row_key, cells
    end
    
    def self.from_params(params)
      from_row(Row.from_params params)
    end
    
    def self.all
      rows = @server.scan @tablename, args: {maxversions: @max_cell_verisons}
      objects = []
      rows.each do |row|
        objects << from_row(row)
      end
      objects
    end
    
    def self.first
      from_row @server.scan(tablename, args: {limit: 1, maxversions: 10})[0]
    end
    
    def self.find(row_key)
      row = @server.scan(tablename, args: {startrow: row_key, limit: 1, maxversions: @max_cell_verisons})[0]
      begin
        from_row row
      rescue StandardError
        raise StandardError.new("Couldn't find #{tablename.singularize} with row_key #{row_key}")
      end
    end
    
    def self.where(*args)
      rows = @server.scan tablename, args: {maxversions: @max_cell_verisons}
      objects = []
      rows.each do |row|
        args.each do |key, val|
          row.cells.each do |cell|
            if cell.column == key and cell.value == val # has what we're looking for
              objects << from_row(row)
            end
          end
        end
      end
      objects
    end
    
    def model_name
      self.class.name
    end
    
    def self.gen_key
      if @default_row_key == 'uuid'
        return SecureRandom.uuid
      elsif @default_row_key == 'auto-increment'
        if self.all.size > 0
          return self.all.last.row_key.to_i + 1
        else
          return 1
        end
      end
    end
    
    def self.set_tablename
      #fix pluralization algorithm
      @tablename = self.name.downcase.pluralize
      #@tablename = tablename
    end
    
    def self.server_url(url, port=nil)
      @server = HRecord::Connection.new(url) if port.nil?
      @server = HRecord::Connection.new(url, port) if !port.nil?
    end
    
    def self.column_families(*cfnames_)
      @cfnames = cfnames_.map {|x| x.to_s}
      self.set_tablename
      if !@cfnames.nil? and !@server.nil?
        self.migrate
        @schema = @server.get_schema(tablename)
      end
      @max_cell_verisons ||= 1
    end
    
#    def self.default_row_key(default_row_key)
#      @default_row_key = default_row_key
#    end
    
    def self.versions(num, only: nil)
#      puts schema
      if num.is_a? Hash
        @max_cell_verisons ||= 1
        num.each do |column_family, versions|
          schema[column_family].versions = versions
          @max_cell_verisons = versions if versions > @max_cell_verisons
        end
      else
        column_families_ = only || @cfnames
        
        if !column_families_.is_a? Array
          column_families_ = [column_families_]
        end
        
        column_families_.map!{|x| x.to_s}

        column_families_.each do |column_family|
  #        puts "Updating schema for column family '#{column_family}'"
  #        puts "#{column_family}: #{@schema[column_family]}"
          schema[column_family].versions = num
        end
        
        @max_cell_verisons = num if num > @max_cell_verisons
      end
      
      @server.update_schema tablename, schema
      @schema = @server.get_schema tablename
    end
    
    def self.tablename
      @tablename
    end
    
    def self.server
      @server
    end
    
    def self.cfnames
      @cfnames
    end
    
    def self.schema 
      @schema
    end
    
    def inspect
      inspect_string = "<#{self.class.name}: "
      @cfnames.each do |cf|
        inspect_string << "@#{cf.pluralize}=#{eval(cf.pluralize)} "
      end
      inspect_string.chomp(" ") << ">"
    end
    
#    def self.post_check
#      begin
#        puts @server 
#      rescue 
#        nil
#      end
#    end
#    
#    post_check
    
  end
  
end
