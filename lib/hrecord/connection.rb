require 'net/http'
require 'base64'
require 'httparty'
#require 'webhdfs-ruby'
require File.join(File.dirname(__FILE__), "hbase_types")
require File.join(File.dirname(__FILE__), "formatter")

module HRecord
  
  class Connection < HRecord::Base
    
    attr_accessor :host
    
    def initialize(hostname='localhost', port=8080)
      @hostname = hostname
      @port = port
      @host = hostname + ':' + port.to_s
      @formatter = HRecord::Formatter
    end
    
#    def scan(tablename, batch=nil, row=nil, col=nil)
#      #col = 'cf:key'
#      uri = @host
#      uri += '/' + tablename + '/scanner'
#      uri = URI(uri)
#      
#      batch = 1 if batch.nil?
#      
#      http = Net::HTTP.new(uri.hostname, uri.port)
#      
#      post = Net::HTTP::Post.new(uri)
#      post.content_type = 'text/xml'
#      post.body = '<Scanner batch="'+batch.to_s+'"/>'
#      
#      scanner = http.request(post)
#      
#      scanner = scanner["location"]
#      scanner = URI(scanner)
#      
#      scan = Net::HTTP::Get.new(scanner)
#      scan["accept"] = 'application/json'
#      scan = http.request(scan)
#      
#      delete = Net::HTTP::Delete.new(scanner)
#      http.request(delete)
#      
#      return scan.body
#    end

    def tables
      response = HTTParty.get(@host, headers: {'Accept'=>'application/json'})
      tables = JSON.parse(response.body)['table']
      tables.map!{|x| x['name']}
    end

    def scan(tablename, args: nil)
      # args = limit,columns,v
      
      uri = @host
      uri += '/' + tablename + '/*'
      uri += '?' + serialize_hash(args) if args
      
      request = HTTParty.get(uri, headers: {'Accept' => 'application/json'})
      #puts JSON.pretty_generate JSON.parse(request.body)
      rows = JSON.parse(request.body)['Row']

      return rows.map { |row| Row.from_h(row) }
    end
    
    def serialize_hash(hash)
      str=''
      hash.each do |k,v|
        str << k.to_s << '=' << v.to_s << '&'
        
      end
      str.chomp('&')
    end
    
    def get_tables
      get
    end
    
    def get(tablename=nil, row=nil, col=nil, timestamp=nil, args: nil)
      #col = 'cf:key'
      # timestamp can be either a number (returns data) or "timestamps" (returns a list of timestamps)
      
      # url? /tablename/row_key/cf:qual,cf:qual/timestamp,timestamp
      
      uri = @host
      uri += '/' + tablename if tablename
      uri += '/schema' if tablename and !row and !col
      uri += '/' + row if row
      uri += '/' + col if col
      uri += '/' + timestamp if timestamp
      uri += '?' + serialize_hash(args) if args
#      uri = URI(uri)
#      
#      response = Net::HTTP.get_response(uri)
#      puts uri
      response = HTTParty.get(uri, headers: {'Accept' => 'application/json'})
      
      formatted = JSON.parse(response.body)
      if !tablename and !row and !col # parse list of tablenames
        formatted = formatted['table'].map {|h| h['name']}
      end
      return formatted
    end
    
    def get_schema(tablename=nil)
      uri = @host+'/'+tablename+'/schema' if !tablename.nil?
      
      response = HTTParty.get uri, headers: {'Accept' => 'application/json'}
      
      puts "GET".cyan.bold << " " << uri
      
      return TableSchema.from_h JSON.parse(response.body)
    end
    
    def update_schema(tablename, schema)
#      puts "schema.to_json => ", schema.to_json
      uri = @host+'/'+tablename+'/schema'
      response = HTTParty.post(uri, body: schema.to_json, headers: {'Content-Type' => 'application/json',
                                                                    'Accept' => 'application/json'})
#      puts "response.body => #{response.body}"                                                              
#      return JSON.parse(response.body)
      return response.code == 200
    end
    
    def put(tablename, *rows)
      xml = "<CellSet>"
      rows.each do |row|
        if !row.row_key.nil?
          xml += "<Row key=\"#{Base64.encode64(row.row_key)}\">"
          row.cells.each do |cell|
#           By convention, a zero timestamp signifies a cell that has never been commited to db.
            if cell.timestamp == 0
              column = Base64.encode64(cell.column)
              value = Base64.encode64(cell.value)
              xml += "<Cell column=\"#{column}\">#{value}</Cell>"
            end
          end
          xml += "</Row>"
        end
      end
      xml += "</CellSet>"
      
#      puts xml
      
      uri = "#{@host}/#{tablename}/fakerow"
      
      response = HTTParty.post(uri, :body => xml, :headers => {
        'Content-Type' => 'text/xml', 
        'Accept'       => 'application/json'
      })
      
      puts "PUT".green.bold << " " << uri
      
      return response == 200
#      return JSON.parse(response.body)
    end
    
#    def put_old(tablename, rows)
##      {
##        'row1'=> {
##          'cf1:key1'=> 'val1',
##          'cf2:key2'=> 'val2'
##        },
##        'row2'=> {
##          'cf3:key3'=> 'val3'
##        }
##      }
#      hbase_upload_path = "/user/hduser/uploads/"
#      
#      xml = '<CellSet>'
#      rows.each do |row|
#        rowkey = row['key']
#        if rowkey != nil
#          row_encoded = Base64.encode64(rowkey)
#          
#          xml += '<Row key="'+row_encoded+'">'
#          cells = row['Cell']
#          cells.each do |cell|
#            col = cell['column']
#            cf  = col.gsub(/:.*/, '')
#            val = cell['$']
#            col_encoded = Base64.encode64(col)
#            xml += '<Cell column="'+col_encoded+'=">'
#            #if ['file', 'image', 'img'].include? cf
#           
##            if val.is_a? ActionDispatch::Http::UploadedFile
##              #data = val.read
##              #content_type = val.content_type.chomp
##              #val_encoded = Base64.encode64(content_type+'|||||'+data)
##              
##              #filename = Time.now.to_s.gsub(/\s-\d{4}/, '').gsub(/\D/,'')+File.extname(val)
##              #path = hbase_upload_path+filename
##              #val_encoded = Base64.encode64(path)
##              #put_file(tablename, path, val)
##              
##              uuid = SecureRandom.uuid
##              put('hfiles', [HRecord::Formatter.new('hfiles',['attr']).params_to_db({
##                'row_key'           => uuid,
##                'attr:data'         => val.read,
##                'attr:extension'    => File.extname(val.open),
##                'attr:content_type' => val.content_type.chomp})])
##              val = uuid
##              
##              
##            #else
##            #  val_encoded = Base64.encode64(val)
##            end
#            val_encoded = Base64.encode64(val)
#            xml += val_encoded
#            xml += '</Cell>'
#          end
#          xml += '</Row>'
#        end
#      end
#      xml += '</CellSet>'
#      
#      uri = @host
#      uri += '/' + tablename
#      uri += '/fakerow'
#      #uri = URI(uri)
#      
#      response = HTTParty.post(uri, 
#                              :body => xml, 
#                              :headers => {'Content-Type' => 'text/xml',
#                                           'Accept'       => 'text/xml'}
#                              )
#      
#      return response
#    end
    
    def put_file(tablename, path, file)
      #path = rows[rows.keys[0]][rows[rows.keys[0]].keys[0]]

      #put(tablename, rows)
      
      hdfs = WebHDFS::Connection.new @hostname, 40070, 'hduser'
      hdfs.create(file, path)
    end
    
    def create(what, tablename, cfnames=nil)
      uri = @host
#      puts @host
      if what == 'table' or what == 'schema'
        uri += "/#{tablename}/schema"
      elsif what == 'scanner'
        uri += "/#{tablename}/scanner"
      end
      #uri = URI(uri)
      
      xml =  "<?xml version='1.0' encoding='UTF-8'?>"
      xml += "<TableSchema name='#{tablename}'>"
      cfnames.each do |cfname|
        xml += "<ColumnSchema name='#{cfname.to_s}' />"
      end
      xml += "</TableSchema>"

      response = HTTParty.post(uri, body: xml, headers: {'Content-Type' => 'text/xml',
                                                         'Accept'       => 'application/json'})
      puts "URI: #{uri}"
      print "Response: #{response.code}(end)\n"
      #return JSON.parse(response.body)
      return response.body
    end
    
    def delete(tablename, row=nil, col=nil)
      uri = "#{@host}/#{tablename}"
      uri += '/schema' if row.nil? and col.nil?
      uri += "/#{row}" if !row.nil?
      uri += "/#{col}" if !col.nil?
      response = HTTParty.delete(uri)
      
      puts "DELETE".red.bold << " " << uri
      
      return response.code == 200
    end
    
    private
    
    def was_successful?(response)
      return response.response.is_a? Net::HTTPOK
    end
    
  end
  
end

#brant@pegasus:~/Desktop/hrecord (master)$ curl -H "Content-Type: text/xml" -d '<Scanner batch="1"/>' http://hbase.davepowerman.com:8080/users/scanner -L -v
#* Hostname was NOT found in DNS cache
#*   Trying 24.154.34.115...
#* Connected to hbase.davepowerman.com (24.154.34.115) port 8080 (#0)
#> POST /users/scanner HTTP/1.1
#> User-Agent: curl/7.35.0
#> Host: hbase.davepowerman.com:8080
#> Accept: */*
#> Content-Type: text/xml
#> Content-Length: 20
#> 
#* upload completely sent off: 20 out of 20 bytes
#< HTTP/1.1 201 Created
#< Location: http://hbase.davepowerman.com:8080/users/scanner/14145512379501d0ee10
#< Content-Length: 0
#< 
#* Connection #0 to host hbase.davepowerman.com left intact
   
#  def issuccessful(self, request):
#    if request.status_code == 200 or request.status_code == 201:
#      return True
#    else:
#      return False
#  

#p = Product.new
#p.attr 'name' 'LED'
#p.save


#products.get 1
#Product.find(1)



#  # give Product a unique key
#p.key = getNextKey()

#p.attr 'name' 'LED'
# -> 1 'attr:name': 'LED'
#p.attr 'price' 0.95
# -> 1 'attr:name': 0.95

#1 'attr:name': 'LED'
