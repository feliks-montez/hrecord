require 'base64'
require 'json'
require 'webhdfs-ruby'
require 'tempfile'
require 'active_support/inflector'

module HRecord

  class Formatter
  
    attr_accessor :tablename, :cfnames, :klass
    
    def initialize(tablename, cfnames, server, klass=nil)
      @tablename = tablename
      @cfnames = cfnames
      @server = server
      @klass = klass || eval(@tablename.singularize.capitalize)
    end
    
    def self.file_to_data(file)
      data = ""
      file.each_line do |line|
        data += line
      end
      return data
    end
  
    def self.json_to_dict(json)
      dict = eval(json.gsub(/:/,'=>'))
      return dict
    end
    
    def self.decode_rows(rows) # pass in json
      rows = (self.json_to_dict rows)["Row"]
      if rows == [] or rows == nil
        rows = []
      else
        rows.each do |row|
          row['key'] = Base64.decode64(row['key'])
          cells = row['Cell']
          cells.each do |cell|
            cell['column'] = Base64.decode64(cell['column'])
            cell['$'] = Base64.decode64(cell['$'])
            if cell['$'] =~ /.*?\|\|\|\|\|.*/
              content_type = cell['$'].gsub(/\|\|\|\|\|.*/, '')
              data = cell['$'].gsub(/.*?\|\|\|\|\|/, '')
              
              #self.code_file(data, content_type)
            end
            #if cell['column'].gsub(/:.*/,'') == 'file'
              #url = WebHDFS::Connection.new('http://buildyourpatent.com', 40070).get_url(cell['$']).to_s
              #cell['$'] = url
            #end
          end
        end
      end
      return rows
    end
    
    def self.hbase_to_dict(hbase)
      rslt = hbase
      keys = []
      rslt.scan(/[^\s]+\s*=>\s*/).each do |i|
        keys += [i.gsub(/=>/, '').gsub(/\s+/, '')]
      end
      keys.each do |key|
        rslt = rslt.gsub(/\s#{key}/, "'#{key}'")
      end
      #rslt = rslt.gsub(/\'+/, '\'')
      return eval(rslt)
    end
    
#    {
#      "key"=>"587b86aa-17b7-4933-bcb7-c1c020b5e0c4",
#      "Cell"=>[
#        {
#          "column"=>"attr:name",
#          "timestamp"=>1416112381601,
#          "$"=>"Thing"
#        },
#        {
#          "column"=>"attr:price",
#          "timestamp"=>1416112381601,
#          "$"=>"9.99"
#        }
#      ]
#    }

    def self.prettify_json(json)
      if json.is_a? Hash
        return JSON.pretty_generate(JSON.parse(json.to_json))
      elsif json.is_a? String
        return JSON.pretty_generate(JSON.parse(json))
      else
        return json
      end
    end

    def params_to_model(params)
#      params['row_key'] ||= @default_row_key
      params['row_key'] ||= SecureRandom.uuid
      db_to_model(params_to_db(params))
    end
    
    def model_to_db(object) 
      row = {}
      cells= []
      row['key'] = object.row_key
      @cfnames.each do |cfname|
        cfdata = eval('object.' + cfname.pluralize)
        cfdata.each do |qual, val|
          cell = {}
          cell['column'] = cfname + ':' + qual
          cell['$'] = val.to_s
          cells += [cell]
        end
      end
      row['Cell'] = cells
      row
    end
    
    def db_to_model(row)
      # db looks like this:
      # [{"key"=>"45fb13b6-3c28-4172-89f6-fc95bf93b93f", "Cell"=>[{"column"=>"attr:color", "timestamp"=>1466122222139, "$"=>"yellow"}, {"column"=>"attr:group", "timestamp"=>1466122222139, "$"=>"fruit"}, {"column"=>"attr:texture", "timestamp"=>1466122222139, "$"=>"slippery"}]}]
      row_key = row['key']
      object = @klass.new #HRecord::Base.new
      object.tablename = @tablename
      object.cfnames   = @cfnames
      object.server    = @server
      object.formatter = self # clone this formatter and gives to new object
      cells = row['Cell']
      cfs = {}
      cells.each do |cell|
        cf = cell['column'].gsub(/:.*/, '')
        qual = cell['column'].gsub(/.*?:/, '')
        val = cell['$']
        timestamp = cell['timestamp']
        # TODO: create {timestamp => val, timestamp2 => val2} for each timestamped version, but default to the most recent
        value = TSValue.new(val, timestamp)
        cfs[cf] ||= {}
        cfs[cf][qual] ||= TSArray.new
        cfs[cf][qual].array.push value
        cfs[cf][qual].sort
        
#        puts "#{cf}:#{qual} => #{cfs[cf][qual]}"
#        cfs[cf] = cfs[cf].merge({qual => value}) unless cfs[cf].include? qual
      end
      @cfnames.each do |cf|
        $attribute_name = cf.pluralize
        
        class << object
          attr_accessor $attribute_name
          
        end  
        if cfs[cf] != {} and cfs[cf] != nil
          eval('object.' + $attribute_name + '=cfs[cf]')
        else
          eval('object.' + $attribute_name + '={}')
        end
      end
      class << object
        attr_accessor 'row_key'
      end
      object.row_key = row_key
      object
    end

    def params_to_db(params={})
      # example params = {'row_key'=>'bmw','attr:color'=>'silver','attr:drive'=>'2WD'}
      # if row_key is not present, this method will add one using a uuid
      row = {}
      cells = []
      
      row['key'] = params['row_key'] || SecureRandom.uuid
      params.each do |col, val|
        if col != 'row_key'
          cell = {}
          cell['column'] = col
          
          cell['$'] = val.to_s
          cells += [cell]
        end
      end
      row['Cell'] = cells
      row
    end
    
  end
  
  class TSValue
    attr_accessor :value, :timestamp
    def initialize(value, timestamp)
      @value = value
      @timestamp = timestamp
    end
    
    def to_s
      value.to_s
    end
    
    def ts
      @timestamp
    end
    
    def inspect
      "<TSValue: ts => #{ts}, value => #{@value}}>"
    end
    
#    def to_i
#      @value.to_i
#    end
#    
#    def to_f
#      @value.to_f
#    end
  end
  
  class TSArray
    attr_accessor :array
  
    def initialize(array=[])
      @array = array
      sort
    end
    
    def sort
      (@array.sort_by! { |x| x.timestamp }).reverse
    end
    
    def to_s
      @array[0].to_s
    end
    
    def [](index)
      @array[index]
    end
    
    def inspect
      "<TSArray: #{@array.inspect}>"
    end
  end
  
end
