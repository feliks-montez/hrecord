require File.join(File.dirname(__FILE__), "formatter")

module HRecord
  class Base
    
    attr_accessor :tablename, :cfnames, :server, :formatter
  
    @max_cell_verisons = 1
  
    def initialize
    end
    
    def tablename=(tablename)
      @tablename = tablename
    end
    
    def cfnames=(cfnames)
      @cfnames = cfnames
    end
    
    def server=(server)
      @server = server
    end
    
    def formatter=(formatter)
      @formatter = formatter
    end
    
    def self.migrate
      #add checking for table already created or in need of updating
      @server.create('table', @tablename, @cfnames)
    end
    
    def self.drop
      @server.delete(@tablename)
    end
    
    def self._new(params={})
      newmod = @formatter.params_to_model(params)
    end
    
    def self.create(params={})
      created = @formatter.params_to_model(params)
      created.save
    end
    
    def update(params={})
      params.each do |key, val|
        cf, qual = key.split(':')
        eval('self.'+cf.pluralize+'["'+qual+'"] = "'+val+'"')
      end
      save
#      new_model = self.class.find @row_key
    end
    
    def destroy
      @server.delete(@tablename, self.row_key)
    end
    
    # TODO: improve this!
    def is_valid?
      valid = false
      
      # valid if not blank
      @cfnames.each do |cf|
        if eval('self.'+cf.pluralize+'.size > 0')
          valid = true
        end
      end
      
      return valid
    end
    
    def save
      if is_valid?
        @server.put(@tablename, [@formatter.model_to_db(self)])
        return true
      else
        return false
      end
    end
    
    def self.all
#      rows = @server.scan self.tablename, args: {maxversions: @max_cell_verisons}
      rows = @server.scan self.tablename
      objects = []
      rows.each do |row|
        objects += [(@formatter.db_to_model row)]
      end
      objects
    end
    
    def self.first
      row = @server.scan(self.tablename, args: {limit: 1})[0]
#      row = @server.scan(self.tablename, args: {limit: 1, maxversions: @max_cell_verisons})[0]
      @model = @formatter.db_to_model row
    end
    
    def self.find(row_key)
      row = @server.scan(self.tablename, args: {startrow: row_key, limit: 1})[0]
#      row = @server.scan(self.tablename, args: {startrow: row_key, limit: 1, maxversions: @max_cell_verisons})[0]
      @model = @formatter.db_to_model row
    end
    
    def self.where(*args)
#      rows = @server.scan self.tablename, args: {maxversions: @max_cell_verisons}
      rows = @server.scan self.tablename
      objects = []
      rows.each do |row|
        args.each do |key, val|
          row['Cell'].each do |cell|
            if cell['column'] == key and cell['$'] == val # has what we're looking for
              objects << @formatter.db_to_model(row)
            end
          end
        end
      end
      objects
    end
    
    def model_name
      self.class.name
    end
    
    def self.gen_key
      if @default_row_key == 'uuid'
        return SecureRandom.uuid
      elsif @default_row_key == 'auto-increment'
        if self.all.size > 0
          return self.all.last.row_key.to_i + 1
        else
          return 1
        end
      end
    end
    
    def self.set_tablename
      #fix pluralization algorithm
      @tablename = self.name.downcase.pluralize
      #@tablename = tablename
    end
    
    def self.server_url(url, port=nil)
      @server = HRecord::Connection.new(url) if port.nil?
      @server = HRecord::Connection.new(url, port) if !port.nil?
    end
    
    def self.column_families(*cfnames)
      tmp = []
      cfnames.each do |column_family|
        tmp.push column_family.to_s
      end
      
      @cfnames = tmp
      self.set_tablename
      @formatter = HRecord::Formatter.new(@tablename, @cfnames, @server, self)
      if !@cfnames.nil? and !@server.nil?
        self.migrate
        ########### ADDDED 06/20/16 ##########
        @schema = @server.get_schema @tablename
        ########################################
      end
    end
    
#    def self.default_row_key(default_row_key)
#      @default_row_key = default_row_key

#    end
        ########### MODIFIED 06/20/16 ##########
    def self.versions(num, only: nil)
      column_families_ = only || @cfnames
      
      if (column_families_.is_a? Array) == false
        column_families_ = [column_families_]
      end
      
      column_families_.each do |column_family|
        puts "Updating schema for column family '#{column_family}'"
        @server.update_schema @tablename, column_family, {:@VERSIONS => num}
      end
      ##########################################
      
      @max_cell_verisons = num
    end
    
    def self.tablename
      @tablename
    end
    
    def self.server
      @server
    end
    
    def self.cfnames
      @cfnames
    end
    
    def self.formatter
      @formatter
    end
    
  end
  
end
