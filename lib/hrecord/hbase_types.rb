require 'base64'
require 'json'

module HRecord

#  class CellVersion
#    attr_accessor :timestamp, :value
##        attr_accessor :column, :timestamp, :value
#    
#    def initialize(value:, timestamp:, column:)
#      @value = value
#      @timestamp = timestamp
#      @column = column
#    end
#    
#    def inspect
#      "<CellVersion #{@value}>"
#    end
#  end

  class Cell
    attr_accessor :value, :timestamp, :column, :versions
    
    def initialize(value: nil, timestamp: nil, column:, versions: nil)
      @versions  = versions || {0=>value} # order versions from oldest to newest
      # set vars based on newest version
      @timestamp = timestamp || newest_version[0]
      @value     = value     || newest_version[1]
      @column    = column
    end
    
    def sorted_versions
      @versions.to_a.sort_by{|x| x[0]}.reverse
    end
    
    def newest_version
      sorted_versions.first
    end
    
    def to_s
      @value
    end
    
    def to_json()
      to_h.to_json
    end
    
    # TODO: make this accept REAL JSON
    def self.from_json(json_versions)
      # @param String json_cells: '[{"timestamp":.., "column":..., "$":...},...]'

      self.from_h JSON.parse(json_versions)
    end
    
    def to_h ()
#      {'column' => Base64.encode64(@column.to_s), 'timestamp' => @timestamp, '$' => Base64.encode64(@value)}
      {'column' => @column.to_s, 'timestamp' => @timestamp, '$' => @value}
    end
    
    def self.from_h(cell_versions, encoded: true)
#      @param Hash|Array cell_versions
      cell_versions = [cell_versions] unless cell_versions.is_a? Array
      column = cell_versions.first['column']
      column = Base64.decode64(column) if encoded
      versions = {}

      cell_versions.each do |cell_item|
#        json = JSON.parse(json) if json.is_a? String
        value = cell_item['$']
        value = Base64.decode64(value) if encoded
        timestamp = cell_item['timestamp'].to_i
        versions[timestamp] = value
      end
      
      Cell.new(column: column, versions: versions)
    end
    
    # NOT always reliable
    def encoded?(str)
      one = (str =~ /[A-Za-z0-9]+={0,3}/) == 0
      two = str.size % 4 == 0
      three = str[-1] == "\n"
      one && two && three
    end
    
    def inspect
#      "<HRecord::Cell @column=#{@column}, @value=#{@value}, @timestamp=#{@timestamp}, @versions=#{@versions}>"
      "<HRecord::Cell #{@column}='#{@value}'>"
    end
  end
  
#  class Column
#    attr_accessor :family, :qualifier, :name
#    
#    def initialize(family: 'cf', qualifier: 'qual')
#      @family = family
#      @qualifier = qualifier
#    end
#    
#    def to_s
#      "#{@family}:#{@qualifier}"
#    end
#    
#    def inspect
#      to_s
#    end
#  end
  
  class Row
    attr_accessor :row_key, :cells
    # TODO: these should not be optional args! fix everywhere!
    def initialize(row_key:, cells:[])
      @row_key = row_key
      @cells = cells
    end
    
    def to_h
      { 
#        'key' => Base64.encode64(@row_key),
        'key' => @row_key,
        'Cell' => cells.map{|cell| cell.to_h}
      }
    end
    
    def to_json
      to_h.to_json
    end
    
    def self.from_json(json)
      self.from_h JSON.parse(json)
    end
    
    def self.from_h(hash)
      row_key = Base64.decode64(hash['key'])
      #puts JSON.pretty_generate(json)
      
      # TODO: handle cell versions that are NOT adjacent
      cells = hash['Cell']
      last_cell = nil
      cell_versions = []
      cell_array = []
      cells.each do |cell|
        if !last_cell.nil? and cell['column'] == last_cell['column'] # cell with same column, aka another version
          cell_versions << cell
        elsif !last_cell # first cell
          cell_versions << cell
        else # cell with new column
          cell_array << cell_versions
          cell_versions = [cell]
        end
        last_cell = cell
      end
      cell_array << cell_versions # add cell versions one last time
      
      cells = cell_array.map { |versions| Cell.from_h versions} # splat versions to pass in each cell as a parameter
      
      Row.new(row_key: row_key, cells: cells)
#      Row.new row_key, cells
    end
    
    def self.from_params(params)
      # example params = {:'row_key'=>'bmw', :'attr:color'=>'silver', :'attr:drive'=>'2WD'}
      # example params = {row_key: 'bmw', attr: {color: 'silver', drive: '2WD'}}
      row_key = params["row_key"]
      params.delete 'row_key'
      cells = []
      params.each do |k, v|
        cells << Cell.new(column: k, value: v)
      end
      
      Row.new(row_key: row_key, cells: cells)     
#      Row.new row_key, cells    
    end
    
    def inspect
      "<HRecord::Row @row_key=#{@row_key}, @cells=#{@cells}>"
    end
  end
  
  class TableSchema
    attr_accessor :name, :column_schemas, :is_meta, :is_root
    
    def initialize(name:, column_schemas:, is_meta: false, is_root: false)
      @name = name
      @column_schemas = column_schemas
      @is_meta = is_meta
      @is_root = is_root
      
#      puts "TableSchema => ", to_h
    end
    
    def [](key)
      column_schemas.each do |cd|
        if cd.name == key
          return cd
        end
      end
      nil
    end
    
    def self.from_h(hash)
#      puts hash
      #c = hash['COLUMNS'].map{|x| ColumnSchema.from_h x}
      c = hash['ColumnSchema'].map{|x| ColumnSchema.from_h x}
      TableSchema.new(
        #name: hash['name'], 
        name: hash['name'],
        column_schemas: c, 
        is_meta: hash['IS_META'] || false, 
        is_root: hash['IS_ROOT'] || false
      )
    end
    
    def self.from_json(json)
      self.from_h JSON.parse(json)
    end
    
    def to_h
      columnSchema = column_schemas.map{|x| x.to_h}
      {
        'name' => name,
        'IS_META' => is_meta,
        'IS_ROOT' => is_root,
        'COLUMNS' => columnSchema
      }
    end
    
    def to_json
      h = to_h
#      h['name'] = h['name']
#      h.delete('name')
#      h['COLUMNS'].each do |col|
#        col['name'] = col['name']
#        col.delete('name')
#      end
      h['ColumnSchema'] = h['COLUMNS']
      h.delete('COLUMNS')
      h.to_json
    end
  end
  
  class ColumnSchema
    attr_accessor(:name, 
                  :block_cache,       
                  :block_size,         
                  :bloom_filter, 
                  :compression, 
                  :data_block_encoding, 
                  :in_memory,         
                  :keep_deleted_cells, 
                  :min_versions,        
                  :replication_scope, 
                  :ttl,       
                  :versions
    )
    
    def initialize(name:, 
                   block_cache: true, 
                   block_size: 65536, 
                   bloom_filter: 'ROW', 
                   compression: 'NONE', 
                   data_block_encoding: 'NONE', 
                   in_memory: false,     
                   keep_deleted_cells: false, 
                   min_versions: 0,
                   replication_scope: 0, 
                   ttl: 'FOREVER', # or maybe 2147483647
                   versions: 1
      )
                   
      @name                = name
      @block_cache         = block_cache
      @block_size          = block_size
      @bloom_filter        = bloom_filter
      @compression         = compression
      @data_block_encoding = data_block_encoding
      @in_memory           = in_memory
      @keep_deleted_cells  = keep_deleted_cells
      @min_versions        = min_versions
      @replication_scope   = replication_scope
      @ttl                 = ttl
      @versions            = versions
      
#      puts "ColumnSchema =>", to_h
    end
    
    def self.from_h(hash)
      ColumnSchema.new(
        name:                hash['name'],
        block_cache:         hash['BLOCKCACHE'] || true,
        block_size:          hash['BLOCKSIZE'] || 65536,
        bloom_filter:        hash['BLOOMFILTER'] || 'ROW',
        compression:         hash['COMPRESSION'] || 'NONE',
        data_block_encoding: hash['DATA_BLOCK_ENCODING'] || 'NONE',
        in_memory:           hash['IN_MEMORY'] || false,
        keep_deleted_cells:  hash['KEEP_DELETED_CELLS'] || false,
        min_versions:        hash['MIN_VERSIONS'] || 0,
        replication_scope:   hash['REPLICATION_SCOPE'] || 0,
        ttl:                 hash['TTL'] || 'FOREVER',
        versions:            hash['VERSIONS'] || 1
      )
    end
    
    def to_h
    {
      'name' => name,
      'BLOCKCACHE' => block_cache,
      'BLOCKSIZE' => block_size,
      'BLOOMFILTER' => bloom_filter,
      'COMPRESSION' => compression,
      'DATA_BLOCK_ENCODING' => data_block_encoding,
      'IN_MEMORY' => in_memory,
      'KEEP_DELETED_CELLS' => keep_deleted_cells,
      'MIN_VERSIONS' => min_versions,
      'REPLICATION_SCOPE' => replication_scope,
      'TTL' => ttl,
      'VERSIONS' => versions
    }
    end
  end
  
#  class TSValue
#    attr_accessor :value, :timestamp
#    def initialize(value, timestamp)
#      @value = value
#      @timestamp = timestamp
#    end
#    
#    def to_s
#      value.to_s
#    end
#    
#    def ts
#      @timestamp
#    end
#    
#    def inspect
#      "<TSValue: ts => #{ts}, value => #{@value}}>"
#    end
#    
##    def to_i
##      @value.to_i
##    end
##    
##    def to_f
##      @value.to_f
##    end
#  end
#  
#  class TSArray
#    attr_accessor :array
#  
#    def initialize(array=[])
#      @array = array
#      sort
#    end
#    
#    def sort
#      (@array.sort_by! { |x| x.timestamp }).reverse
#    end
#    
#    def to_s
#      @array[0].to_s
#    end
#    
#    def [](index)
#      @array[index]
#    end
#    
#    def inspect
#      "<TSArray: #{@array.inspect}>"
#    end
#  end

end
