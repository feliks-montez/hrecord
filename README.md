# HRecord

An HBase interface for Ruby

## Installation

- Obtain the latest version of the repository.
- Run ```gem build hrecord.gemspec```
- Run ```gem install hrecord-<version #>.gem```
- Done!
